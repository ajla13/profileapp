package si.uni_lj.fri.pbd.lab2;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class ProfileActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        Intent intent = getIntent();
        String fullName = intent.getStringExtra(RegistrationActivity.EXTRA_NAME);
        TextView mTextView = (TextView) findViewById(R.id.user_name);
        mTextView.setText(fullName);
        Button msgButton=(Button) findViewById(R.id.show_button);
        msgButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Context context = getApplicationContext();
                EditText editMsg = (EditText) findViewById(R.id.editText);
                String msg = editMsg.getText().toString();
                int duration = Toast.LENGTH_LONG;
                Toast toast = Toast.makeText(context, msg, duration);
                toast.show();
            }
        });
    }
}
