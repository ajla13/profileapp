package si.uni_lj.fri.pbd.lab2;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.view.View;
import android.widget.EditText;

public class RegistrationActivity extends AppCompatActivity {

    public static String EXTRA_NAME;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration);
    }
    public void registerUser(View view, String errMsg){
        EditText name=(EditText)findViewById(R.id.edit_name);
        if(name.getText().toString().length()<=0){
            name.setError(errMsg);
        }
        else{
            name.setError(null);
            Intent intent = new Intent(this, ProfileActivity.class);
            intent.putExtra(EXTRA_NAME, (Parcelable) name);
            startActivity(intent);
        }
    }
}
